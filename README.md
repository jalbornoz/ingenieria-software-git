# ingenieria-software-git

Repositorio destinado a alojar activos generados como instancias de aprendizaje en la asignatura del software 2.

# Consignas


1. Descargar algún cliente git
1. Configurar workspace
1. Crear una rama desde el master y editar el presente documento
1. Realizar un marge al master
1. Crear un repositorio nuevo
1. Crear y desplegar estructura de directorios con documentos: *.txt o *.md, con detalle de lo que sea deberá almancenar allí
1. Clonar los repositorios de los demás compañeros de clases.
1. Editar archivos y realizar push al master o rama correspondiente.
